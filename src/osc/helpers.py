from utils import dict_traverse
import os

__all__ = ['dict_to_osc_tuple']

def dict_to_osc_tuple(data: dict) -> list:
    return [('/' + os.path.join(*res[0]), res[1]) for res in dict_traverse(data)]