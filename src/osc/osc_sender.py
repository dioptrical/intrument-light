from pythonosc import osc_message_builder
import collections
import numpy as np
from .client import Client

class OscSender:
    
    def __init__(self):
        self._is_streaming = False

    def send(self, address: str, client: Client, data:any) -> None:

        msg = osc_message_builder.OscMessageBuilder(address=address)

        if isinstance(data, (collections.abc.Sequence, np.ndarray)):
            for item in data:
                msg.add_arg(item)
        else:
            msg.add_arg(data)
        
        msg = msg.build()

        tmp_udp_client = client.udp
        tmp_udp_client.send(msg)
        
    
    @property
    def is_streaming(self) -> bool:
        return self._is_streaming
    
    @is_streaming.setter
    def is_streaming(self, is_streaming: bool):
        self._is_streaming = is_streaming