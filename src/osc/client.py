from pythonosc import udp_client

class Client:

    def __init__(self, ip: str, port: int):
        self.ip = ip
        self.port = port
        self.udp = udp_client.UDPClient(ip, port)

