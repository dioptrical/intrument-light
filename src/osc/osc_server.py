from pythonosc import osc_server
from pythonosc import dispatcher
import asyncio
import threading

class OscServer:

    def __init__(self, local_address: str, local_port: int):
        self._ip = local_address
        self._port = local_port
        self._server_loop = asyncio.get_event_loop()
        self._server = None
        self._dispatcher = None
        self._server_thread = None

    def build(self, dispatcher: dispatcher.Dispatcher):
        self._dispatcher = dispatcher
        self._server = osc_server.ThreadingOSCUDPServer((self._ip, self._port), dispatcher, self._server_loop)
        self._server_thread = threading.Thread(daemon=True, target=self._server.serve_forever)

    def start(self):
        self._server_thread.start()
        print(f"Serving at {self._server.server_address[0]} port: {self._server.server_address[1]}")


    @property
    def dispatcher(self):
        return self._dispatcher