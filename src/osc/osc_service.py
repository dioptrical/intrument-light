from pythonosc import dispatcher
from .osc_sender import OscSender
from .osc_server import OscServer
from .client import Client

class OscService:

    def __init__(self, config):
        self._dispatcher = dispatcher.Dispatcher()
        self._osc_sender = OscSender()
        self._osc_server = OscServer(config['local-address'], int(config['local-port']))
        self._osc_server.build(self._dispatcher)
        self._config: dict = config
        self._clients: list[Client] = []

    def start_server_thread(self):
        self._osc_server.start()

    def attach_callback(self, address: str, callback):
        self._dispatcher.map(address, callback)

    def detach_callback(self, address: str, callback: callable):
        self._dispatcher.unmap(address, callback)

    def attach_client(self, address: str, *args: list[any]) -> None:
        if len(list(filter(lambda x: x.ip == args[0] and x.port == args[1], self._clients))):
            print('client already exists')
            return
        tmp_client = Client(args[0], int(args[1]))
        print(f"Added Client: {tmp_client.ip}:{tmp_client.port}")
        self._clients.append(tmp_client)

    def detach_client (self, address: str, *args: list[any]) -> None:
        self._clients[:] = [client for client in self._clients if client.ip != args[0] and client.port != args[1]]

    def send(self, address: str, data:any):
        for client in self._clients:
            self._osc_sender.send(address, client, data)