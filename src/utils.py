import re

__all__ = ['dict_traverse', 'to_upper_camel_case']


def dict_traverse(data, path=[]):
    path = path[:] if path else []
    if isinstance(data, dict):
        for key, value in data.items():
            if isinstance(value, dict):
                for rec in dict_traverse(value, path + [key]):
                    yield rec
            elif isinstance(value, list) or isinstance(value, tuple):
                for v in value:
                    for rec in dict_traverse(v, path + [key]):
                        yield rec
            else:
                yield (path + [key], value)
    else:
        yield path, data
        

def to_upper_camel_case(string):
    return re.sub(r"(_|-)+", " ", string).title().replace(" ", "")
