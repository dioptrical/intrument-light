import toml
import cv2
import osc
from queue import Queue
import vision.videoIO as vio
import vision.detector as vd
from time import time

capture_queue: Queue
osc_service: osc.OscService
video_capture: vio.VideoGrabber
video_displayer: vio.VideoDisplayer
detection_service: vd.DetectionService

font = cv2.FONT_HERSHEY_SIMPLEX
last_detection_time = 0
fps = 0

previous_left_coords = {
    'x': 0,
    'y': 0
}

previous_right_coords = {
    'x': 0,
    'y': 0
}

def calculate_velocity(fps: int, current_coords: dict, previous_coords: dict):
    return (
        abs(current_coords['x'] - previous_coords['x']) * fps,
        abs(current_coords['y'] - previous_coords['y']) * fps
    )

def start():
    osc_service.start_server_thread()
    video_capture.start_capture_thread()
    detection_service.start_detection_thread()
    video_displayer.start_display_thread()


def clean():
    cv2.destroyAllWindows()
    video_capture.stop_capture_thread()
    video_displayer.stop_display_thread()
    detection_service.stop_detection_thread()

def process_thread_message(message, emitter: any):
    if message == 'panic' or message == 'interrupt':
        clean()

def calculateFPS() -> int:
    present = time()
    global last_detection_time
    tmp_fps = int(1 / (present - last_detection_time))
    last_detection_time = present
    return tmp_fps


def process_detection_result(result: vd.DetectionResult):
    global fps
    fps = calculateFPS()
    cv2.putText(result.frame, f"Process FPS: {fps}", (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)

    video_displayer.display(result.frame)
    
    if not result.data:
        return
    else:
        result.data['fps'] = fps

        for detected in result.data['hand']:
            if 'right' in detected.keys():
                velocity = calculate_velocity(fps, detected['right']['wrist']['coords'], previous_right_coords)
                previous_right_coords['x'] = detected['right']['wrist']['coords']['x']
                previous_right_coords['y'] = detected['right']['wrist']['coords']['y']
                detected['right']['wrist']['velocity'] = {}
                detected['right']['wrist']['velocity']['x'] = velocity[0]
                detected['right']['wrist']['velocity']['y'] = velocity[1]
            if 'left' in detected.keys():
                velocity = calculate_velocity(fps, detected['left']['wrist']['coords'], previous_right_coords)
                previous_right_coords['x'] = detected['left']['wrist']['coords']['x']
                previous_right_coords['y'] = detected['left']['wrist']['coords']['y']
                detected['left']['wrist']['velocity'] = {}
                detected['left']['wrist']['velocity']['x'] = velocity[0]
                detected['left']['wrist']['velocity']['y'] = velocity[1]

        for item in osc.helpers.dict_to_osc_tuple(result.data):
            print(item)
            osc_service.send(item[0], item[1])


def init():
    with open('./config/config.toml', 'r') as global_config:
        global_config = toml.load(global_config)
    
    window_config = global_config['window']
    osc_config = global_config['osc']
    capture_config = global_config['capture']
    detector_config = global_config['detector']

    global osc_service
    osc_service = osc.OscService(osc_config)
    osc_service.attach_callback('/subscribe', osc_service.attach_client)
    osc_service.attach_callback('/forget', osc_service.detach_client)

    global capture_queue
    capture_queue = Queue()

    global video_capture
    video_capture = vio.VideoGrabber(capture_config['camera-number'], capture_queue, process_thread_message)

    global detection_service
    detection_service = vd.DetectionService(detector_config, process_detection_result, capture_queue)

    global video_displayer
    video_displayer = vio.VideoDisplayer(window_config['name'], process_thread_message)

    # start all threads
    start()
    

if __name__ == '__main__':
    init()
