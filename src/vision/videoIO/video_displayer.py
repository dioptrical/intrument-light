from threading import Thread
import cv2
import time
from descriptor import SetterProperty
import numpy as np
from typing import Callable

class VideoDisplayer:

    def __init__(self, window_name: str, msg_callback: Callable):
        self._msg_callback = msg_callback
        self._display_thread = Thread(target=self._display_loop, args=())
        self._display_thread.daemon = True
        self._is_running = False
        self._window_name = window_name
        self._frame = np.full((640, 480, 3) , (255, 0, 0), np.uint8)

    def _display_loop(self):
        while self._is_running:
            cv2.imshow(self._window_name, self._frame)
            if cv2.waitKey(1) == ord('q'):
               self._msg_callback('interrupt', self)
        # time.sleep(0.01)

    def display(self, frame):
        self._frame = frame


    def start_display_thread(self):
        if not self._display_thread.is_alive():
            self._is_running = True
            self._display_thread.start()
            return self
        else:
            raise Exception("Display thread already running")
        
    def stop_display_thread(self):
        self._is_running = False

    def __setattr__(self, name, value):
        if name == 'frame':
            self._frame = value
        super(VideoDisplayer, self).__setattr__(name, value)

    @property
    def is_thread_running(self):
        return self._display_thread.is_alive()
    