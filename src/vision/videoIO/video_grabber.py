import cv2
import threading
import queue
import time
import warnings
from typing import Callable

class CaptureQueueEmptyWarning(UserWarning):
    pass

class VideoGrabber:

    def __init__(self, device: int, capture_queue: queue.Queue, msg_callback: Callable):
        self._msg_callback = msg_callback
        self._capture = cv2.VideoCapture(device)            
        self._is_grabbed, _ = self._capture.read()
        self._capture_thread = threading.Thread(target=self._capture_loop, args=())
        # self._capture_thread.daemon = True
        self._is_paused:bool = False
        self._is_running: bool
        self._capture_queue = capture_queue
        self._fps = 0
        print(f"Capture size: {int(self._capture.get(cv2.CAP_PROP_FRAME_WIDTH))}x{int(self._capture.get(cv2.CAP_PROP_FRAME_HEIGHT))}")

    def _capture_loop(self):

        while self._is_running == True and self._is_grabbed == True:
            if not self._is_paused:
                self._is_grabbed, frame = self._capture.read()
                frame = cv2.flip(frame, 1)
                cv2.putText(frame, f"Capture native FPS {self._fps}", (20, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)

                try:
                    self._capture_queue.get_nowait()
                except queue.Empty:
                    warnings.warn('Queue empty', CaptureQueueEmptyWarning)
                self._capture_queue.put(frame)
                if self._is_grabbed:
                    self._fps = self._capture.get(cv2.CAP_PROP_FPS)
                else:
                    self.stop_capture_thread()

    def start_capture_thread(self):
        if not self._capture_thread.is_alive():
            self._is_running = True
            self._capture_thread.start()
            return self
        else:
            raise Exception("Capture thread already running")
        
    def stop_capture_thread(self):
        self._is_running = False
        self._capture.release()


    @property
    def is_paused(self):
        return self._is_paused
    
    @is_paused.setter
    def is_paused(self, is_paused):
        self._is_paused = is_paused
    
    @property
    def fps(self) -> float:
        return self._fps
