import vision.detector.detector_class as dc
import vision.detector.detection_result as dr
import numpy as np
import threading
from utils import to_upper_camel_case
import importlib
from queue import Queue
from typing import Callable

class DetectionService:

    def __init__(self, config: dict, callback: Callable, capture_queue:Queue):
        self.config:dict = config
        self.detectors = []
        for detector_name in config['loaded']:
            try:
                loaded_module = importlib.import_module(f"{dc.__name__}.{detector_name}", dc.__name__)
                loaded_class = getattr(loaded_module, to_upper_camel_case(detector_name))
                self.detectors.append(loaded_class(config[detector_name]))
            except Exception:
                raise NameError(f"Couldn't load class {to_upper_camel_case(detector_name)}. Class not found")

        self.capture_queue = capture_queue
        self.detection_thread = threading.Thread(target=self._detection_loop, args=())
        self.is_running: bool
        self.callback = callback

    def _detection_loop(self):
        while self.is_running:
            if not self.capture_queue.empty():
                tmp_frame = self.capture_queue.get()
                for detector in self.detectors:
                    result: dr.DetectionResult = detector.detect(tmp_frame)
                    self.callback(result)

    def start_detection_thread(self):
        self.is_running = True
        self.detection_thread.start()
        return self

    def stop_detection_thread(self):
        self.is_running = False
