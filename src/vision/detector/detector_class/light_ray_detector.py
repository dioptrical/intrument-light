from .abstract_detector import AbstractDetector
import vision.detector.detection_result as dr

class LightRayDetector(AbstractDetector):

    def __init__(self, config: dict):
        super().__init__()

    def detect(self, frame) -> dr.DetectionResult:
        return dr.DetectionResult()