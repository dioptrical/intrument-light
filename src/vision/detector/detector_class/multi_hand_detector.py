from .abstract_detector import AbstractDetector
import vision.detector.detection_result as dr
import vision.detector.detection_result as dr
import cv2
import mediapipe as mp
from google.protobuf.json_format import MessageToDict
import numpy as np


class MultiHandDetector(AbstractDetector):

    def __init__(self, config: dict):
        super().__init__()
        self._config = config
        self._mp_hands = mp.solutions.hands # type: ignore
        self._mp_drawing_utils = mp.solutions.drawing_utils # type: ignore
        self._hands = self._mp_hands.Hands (
            static_image_mode=config['static_image_mode'],
            model_complexity=config['model_complexity'],
            min_detection_confidence=config['min_detection_confidence'],
            min_tracking_confidence=config['max_detection_confidence'],
            max_num_hands=config['max_num_hands']
        )
        self._detection_result = dr.DetectionResult()

    def detect(self, frame: np.ndarray) -> dr.DetectionResult:
        frame_height, frame_width, _ = frame.shape
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        result = self._hands.process(frame)

        self._detection_result.data = {}

        if result.multi_hand_landmarks:
            self._detection_result.data = {'hand': []}
            for idx, hand in enumerate(result.multi_handedness):
                label = MessageToDict(hand)['classification'][0]['label']
                current_landmarks = result.multi_hand_landmarks[idx]
                hand_landmarks_data = {label.lower(): {}}
                for tracked_landmark in self._config['tracked_landmarks']:
                    x = current_landmarks.landmark[self._mp_hands.HandLandmark[tracked_landmark]].x * frame_width
                    y = current_landmarks.landmark[self._mp_hands.HandLandmark[tracked_landmark]].y * frame_height
                
                    hand_landmarks_data[label.lower()][tracked_landmark.lower()] = {
                        'coords': {
                            'x': x,
                            'y': y
                        }
                    }
                    self._mp_drawing_utils.draw_landmarks(frame, current_landmarks, self._mp_hands.HAND_CONNECTIONS)
                    cv2.putText(frame, f"{label.lower()} {tracked_landmark.lower()}", (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                    
                self._detection_result.data['hand'].append (hand_landmarks_data) 

        self._detection_result.frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return self._detection_result