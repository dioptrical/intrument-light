from .abstract_detector import AbstractDetector
import numpy as np
import vision.detector.detection_result as dr
import cv2

class ColorMarkerDetector(AbstractDetector):

    def __init__(self, config):
        super().__init__()
        self._config = config
        self._detection_result = dr.DetectionResult()
        self._colors = config['colors']
        self._detection_result.data = {}
        self._blob_data_template = {
            'coords': { 'x': float, 'y': float },
            'dimension': { 'width': float, 'height': float }
        }


    def detect(self, frame: np.ndarray) -> dr.DetectionResult:

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        kernel = np.ones((5, 5), 'uint8')
        self._detection_result.data[self._config['osc_root_address']] = []

        frame = cv2.GaussianBlur(frame, (9, 9), cv2.BORDER_DEFAULT)

        for color in self._colors:
            tmp_lower = np.array(color['lower'], np.uint8)
            tmp_upper = np.array(color['upper'], np.uint8)
            mask = cv2.inRange(hsv, tmp_lower, tmp_upper)
            mask = cv2.dilate(mask, kernel)
            res = cv2.bitwise_and(frame, frame, mask = mask)
            contours, hierarchy = cv2.findContours(mask,
                                           cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)
            if contours:
                for i, contour in enumerate(contours):
                    tmp_data_dict = { str(i): { color['name']: self._blob_data_template}}
                    
                    # print(tmp_data_dict)
                    area = cv2.contourArea(contour)
                    if(self._config['min_contour_area'] < area < self._config['max_contour_area']):
                        
                        x, y, w, h = cv2.boundingRect(contour)
                        tmp_data_dict[str(i)][color['name']]['coords']['x'] = x
                        tmp_data_dict[str(i)][color['name']]['coords']['y'] = y
                        tmp_data_dict[str(i)][color['name']]['dimension']['width'] = w
                        tmp_data_dict[str(i)][color['name']]['dimension']['height'] = h

                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), color['rect_color'], 2)
                        
                        cv2.putText(frame, f"{color['name']}", (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color['rect_color'])
                        self._detection_result.data[self._config['osc_root_address']].append(tmp_data_dict)  
                    
        self._detection_result.frame = frame
        return self._detection_result

