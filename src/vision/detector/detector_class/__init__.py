# warning: the classes are loaded by name. The snake_case from config.toml is transformed to UpperCamelCase to build the class' name
# please provide correctly formatted names or a NameError will be raised

from os.path import dirname, basename, isfile, join
import glob


__all__ =   [
            basename(module_name)[:-3] 
            for module_name 
            in glob.glob(join(dirname(__file__), '*.py')) 
            if isfile(module_name) and not module_name.endswith('__init__.py')
            ]

