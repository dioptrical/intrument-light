import numpy as np

from abc import ABC, abstractmethod

class AbstractDetector(ABC):

    def __init__(self):
        self._lock:bool = False

    @abstractmethod
    def detect(self, frame) -> dict:
        pass

    @property
    def lock(self) -> bool:
        return self._lock