from dataclasses import dataclass
import numpy as np

@dataclass
class DetectionResult:
    
    def __init__(self):
        self.frame: np.ndarray
        self.data: dict = {}